import React, {useContext, useState} from 'react';
import { View, Button, StyleSheet, TextInput, Text } from 'react-native';
import AuthContext from '../../contexts/auth';

const SignIn: React.FC = () => {
  const {signIn} = useContext(AuthContext);
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  function handleSignIn(){
    signIn(email, password);
  }

  function onChangeEmail(data: any) {
    setEmail(data);
  }

  function onChangePassword(data: any) {
    setPassword(data);
  }

  return(
    <View style={styles.container}>
      <Text style={styles.span}>Email</Text>
      <TextInput style={styles.input} value={email}
        onChangeText={text => onChangeEmail(text)}></TextInput>
      <Text style={styles.span}>Senha</Text>
      <TextInput style={styles.input} value={password}
        onChangeText={text => onChangePassword(text)}></TextInput>
      <View style={styles.button}>
        <Button title='Entrar' onPress={handleSignIn}></Button>
      </View>
      <View style={styles.button}>
        <Button title='Register' onPress={handleSignIn}></Button>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 50,
    paddingRight: 50,
  },
  input:{
    marginBottom: 20,
    color: 'white',
    borderBottomColor: 'white',
    borderWidth: 1, 
  },
  span:{
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold',
  },
  button:{
    paddingTop: 5,
    paddingBottom: 5,
  }
})

export default SignIn;