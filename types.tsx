export type RootStackParamList = {
  SignIn: undefined;
  Root: undefined;
};

export type BottomTabParamList = {
  Viagem: undefined;
  Rota: undefined;
};

export type TabOneParamList = {
  TripScreen: undefined;
};

export type TabTwoParamList = {
  PathScreen: undefined;
};
